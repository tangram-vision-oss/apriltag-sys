#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
// We do this for bindgen
#![allow(clippy::missing_safety_doc)]
// We do this because apriltag is not good with the pointers
#![allow(deref_nullptr)]

include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/bindings",
    "/bindings.rs"
));
